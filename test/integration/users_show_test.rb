require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest

  def setup
    @user     = users(:michael)
    @inactive = users(:ethan)
  end

  test "should not show inactive user" do
    log_in_as(@user)
    get user_path(@inactive)
    assert_redirected_to root_url
  end
end
